var HttpDispatcher = require('httpdispatcher');
const http = require('http');
const fs = require('fs');
const path = require('path');
var dispatcher     = new HttpDispatcher();

const casePath = 'case.json';
const chartPath = 'chart.json';
const mapPath = 'map.json';

const server = http.createServer(handleRequest);
// const server = http.createServer((request, response) => {
//   	fs.readFile(filePath, 'utf8', function (err, data) {
// 		if (err) throw err;
// 		try {
// 			obj = JSON.parse(data);
// 			response.writeHead(200, {"Content-Type": "application/json"});
// 			response.write(data);
// 			response.end();
// 		} catch (e) {
// 			console.error( e );
// 		}
// 	});
// });
// 
// We need a function which handles requests and send response
function handleRequest(request, response){
    try {
        // log the request on console
        console.log(request.url);
        // Dispatch
        dispatcher.dispatch(request, response);
    } catch(err) {
        console.log(err);
    }
}

//A sample GET request
dispatcher.onGet("/case-report", function(req, res) {
    fs.readFile(path.join(__dirname, casePath), 'utf8', function (err, data) {
		if (err) throw err;
		try {
			// obj = JSON.parse(data);
			res.writeHead(200, {"Content-Type": "application/json"});
			res.write(data);
			res.end();
		} catch (e) {
			console.error( e );
		}
	});
});

dispatcher.onGet("/chart-report", function(req, res) {
    fs.readFile(path.join(__dirname, chartPath), 'utf8', function (err, data) {
		if (err) throw err;
		try {
			// obj = JSON.parse(data);
			res.writeHead(200, {"Content-Type": "application/json"});
			res.write(data);
			res.end();
		} catch (e) {
			console.error( e );
		}
	});
});

dispatcher.onGet("/map-report", function(req, res) {
    fs.readFile(path.join(__dirname, mapPath), 'utf8', function (err, data) {
		if (err) throw err;
		try {
			// obj = JSON.parse(data);
			res.writeHead(200, {"Content-Type": "application/json"});
			res.write(data);
			res.end();
		} catch (e) {
			console.error( e );
		}
	});
});

dispatcher.onError(function(req, res) {
    res.writeHead(404);
    res.end("Error, the URL doesn't exist");
});

const port = process.env.PORT || 3000;
server.listen(port, (e) => console.log(e ? 'Oops': `Server running on http://localhost:${port}`));

// var express = require("express");
// var app = express();

// app.listen(3000, () => {
//  console.log("Server running on port 3000");
// });

// app.get("/url", (req, res, next) => {
//  res.json(["Tony","Lisa","Michael","Ginger","Food"]);
// });
