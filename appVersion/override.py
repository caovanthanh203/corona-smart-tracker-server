# -*- coding: utf-8 -*-
# @Author: Cao Van Thanh
# @Date:   2020-03-28 14:07:31
# @Last Modified by:   Cao Van Thanh
# @Last Modified time: 2020-03-28 14:10:42
import json

app_version_code = 9
app_url = 'https://forum.xda-developers.com/android/apps-games/coronavirus-tracker-android-corona-t4061875'

with open('_case.json') as json_file:
    data = json.load(json_file)
    data['app_version_code'] = app_version_code
    data['app_url'] = app_url

with open('_case_overrided.json', 'w') as outfile:
   	json.dump(data, outfile)
