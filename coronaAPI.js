var HttpDispatcher = require('httpdispatcher');
var cache = require('memory-cache');
const http = require('http');
const fs = require('fs');
const path = require('path');
var dispatcher     = new HttpDispatcher();

const casePath = 'case.json';
const chartPath = 'chart.json';
const mapPath = 'map.json';

const cacheTime = 300000;

const server = http.createServer(handleRequest);

// We need a function which handles requests and send response
function handleRequest(request, response){
    try {
        // log the request on console
        console.log(request.url);
        // Dispatch
        dispatcher.dispatch(request, response);
    } catch(err) {
        console.log(err);
    }
}

//A sample GET request
dispatcher.onGet("/case-report", function(req, res) {
	cachedCase = cache.get('case-report-cache');
	if (cachedCase == null){
			fs.readFile(path.join(__dirname, casePath), 'utf8', function (err, data) {
				console.log("from file");
				if (err) throw err;
				try {
					cache.put('case-report-cache', data);
					res.writeHead(200, {"Content-Type": "application/json"});
					res.write(data);
					res.end();
				} catch (e) {
				console.error( e );
			}
		});
	} else {
		console.log("from cache");
		res.writeHead(200, {"Content-Type": "application/json"});
		res.write(cachedCase);					
		res.end();
	}
});

dispatcher.onGet("/chart-report", function(req, res) {
	cachedChart = cache.get('chart-report-cache');
	if (cachedChart == null){
			fs.readFile(path.join(__dirname, chartPath), 'utf8', function (err, data) {
				console.log("from file");
				if (err) throw err;
				try {
					cache.put('chart-report-cache', data);
					res.writeHead(200, {"Content-Type": "application/json"});
					res.write(data);
					res.end();
				} catch (e) {
				console.error( e );
			}
		});
	} else {
		console.log("from cache");
		res.writeHead(200, {"Content-Type": "application/json"});
		res.write(cachedChart);					
		res.end();
	}
});

dispatcher.onGet("/map-report", function(req, res) {
    cachedMap = cache.get('map-report-cache');
	if (cachedMap == null){
			fs.readFile(path.join(__dirname, mapPath), 'utf8', function (err, data) {
				console.log("from file");
				if (err) throw err;
				try {
					cache.put('map-report-cache', data);
					res.writeHead(200, {"Content-Type": "application/json"});
					res.write(data);
					res.end();
				} catch (e) {
				console.error( e );
			}
		});
	} else {
		console.log("from cache");
		res.writeHead(200, {"Content-Type": "application/json"});
		res.write(cachedMap);					
		res.end();
	}
});

dispatcher.onGet("/case-cache-update", function(req, res) {
    cachedCasePrivate = cache.get('case-report-cache-private');
	if (cachedCasePrivate == null){
			fs.readFile(path.join(__dirname, casePath), 'utf8', function (err, data) {
				console.log("from file");
				if (err) throw err;
				try {
					cache.put('case-report-cache-private', data, cacheTime);
					cache.put('case-report-cache', data);
					res.end();
				} catch (e) {
				console.error( e );
			}
		});
	} else {
		res.end();
	}
});

dispatcher.onGet("/chart-cache-update", function(req, res) {
    cachedChartPrivate = cache.get('chart-report-cache-private');
	if (cachedChartPrivate == null){
			fs.readFile(path.join(__dirname, chartPath), 'utf8', function (err, data) {
				console.log("from file");
				if (err) throw err;
				try {
					cache.put('chart-report-cache-private', data, cacheTime);
					cache.put('chart-report-cache', data);
					res.end();
				} catch (e) {
				console.error( e );
			}
		});
	} else {
		res.end();
	}
});

dispatcher.onGet("/map-cache-update", function(req, res) {
    cachedMapPrivate = cache.get('map-report-cache-private');
	if (cachedMapPrivate == null){
			fs.readFile(path.join(__dirname, mapPath), 'utf8', function (err, data) {
				console.log("from file");
				if (err) throw err;
				try {
					cache.put('map-report-cache-private', data, cacheTime);
					cache.put('map-report-cache', data);
					res.end();
				} catch (e) {
				console.error( e );
			}
		});
	} else {
		res.end();
	}
});

dispatcher.onGet("/force-case-cache-update", function(req, res) {
    cachedCasePrivate = cache.get('case-report-cache-private');
	if (cachedCasePrivate != null){
			fs.readFile(path.join(__dirname, casePath), 'utf8', function (err, data) {
				console.log("from file");
				if (err) throw err;
				try {
					cache.put('case-report-cache-private', data, cacheTime);
					cache.put('case-report-cache', data);
					res.end();
				} catch (e) {
				console.error( e );
			}
		});
	} else {
		res.end();
	}
});

dispatcher.onGet("/force-chart-cache-update", function(req, res) {
    cachedChartPrivate = cache.get('chart-report-cache-private');
	if (cachedChartPrivate != null){
			fs.readFile(path.join(__dirname, chartPath), 'utf8', function (err, data) {
				console.log("from file");
				if (err) throw err;
				try {
					cache.put('chart-report-cache-private', data, cacheTime);
					cache.put('chart-report-cache', data);
					res.end();
				} catch (e) {
				console.error( e );
			}
		});
	} else {
		res.end();
	}
});

dispatcher.onGet("/force-map-cache-update", function(req, res) {
    cachedMapPrivate = cache.get('map-report-cache-private');
	if (cachedMapPrivate != null){
			fs.readFile(path.join(__dirname, mapPath), 'utf8', function (err, data) {
				console.log("from file");
				if (err) throw err;
				try {
					cache.put('map-report-cache-private', data, cacheTime);
					cache.put('map-report-cache', data);
					res.end();
				} catch (e) {
				console.error( e );
			}
		});
	} else {
		res.end();
	}
});

dispatcher.onGet("/loaderio-3a53a1094981fadb96fdecc404125f4a/", function(req, res) {
    res.writeHead(200, {"content-type": "text/html; charset=utf-8"});
    res.write("loaderio-3a53a1094981fadb96fdecc404125f4a");
    res.end();
    //cachedMapPrivate = cache.get('map-report-cache-private');
    //    if (cachedMapPrivate == null){
    //                    fs.readFile(path.join(__dirname, mapPath), 'utf8', function (err, data) {
    //                            console.log("from file");
    //                            if (err) throw err;
    //                            try {
    //                                    cache.put('map-report-cache-private', data, cacheTime);
    //                                    cache.put('map-report-cache', data);
    //                                    res.end();
    //                            } catch (e) {
    //                            console.error( e );
    //                    }
    //            });
    //    } else {
    //            res.end();
    //    }
});

dispatcher.onError(function(req, res) {
    res.writeHead(404);
    res.end("Error, the URL doesn't exist");
});

const port = process.env.PORT || 3000;
server.listen(port, (e) => console.log(e ? 'Oops': `Server running on http://localhost:${port}`));
