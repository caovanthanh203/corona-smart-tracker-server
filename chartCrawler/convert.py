# -*- coding: utf-8 -*-
# @Author: Cao Van Thanh
# @Date:   2020-03-26 01:40:56
# @Last Modified by:   Cao Van Thanh
# @Last Modified time: 2020-03-28 11:05:00
import csv
import time 
import datetime
import json

caseCsvPath='_raw.csv'
deathCsvPath='_death.csv'
recoveredCsvPath='_recovered.csv'

cols = {}
confirms = {"features": []}
deaths = {}
recovereds = {}
# attribute = {}
# death = {}
with open(caseCsvPath) as caseCsv:
    csvReader = csv.DictReader(caseCsv)
    cols = csvReader.fieldnames
    for index, col in enumerate(cols, start=0):
    	if (index > 3):
            date = datetime.datetime.strptime(col,"%m/%d/%y")
            timestamp = time.mktime(date.timetuple())*1000
            sum_total = 0
            sum_china = 0
            sum_world = 0
            attribute = {}
            for row_index, row in enumerate(csvReader, start=0):
            	if (row_index > 0):
            	    sum_total = sum_total + int(row[col])
            	    if (row[cols[1]] == 'China'):
            	    	sum_china = sum_china + int(row[col])
            	    else:
            	    	sum_world = sum_world + int(row[col])
            caseCsv.seek(0)
            attribute['Report_Date'] = timestamp
            attribute['Mainland_China'] = sum_china
            attribute['Other_Locations'] = sum_world
            attribute['Total_Confirmed'] = sum_total
            # attribute['Death'] = sum_china
            # attribute['Recovered'] = sum_world
            attribute['Confirmed'] = sum_total
            attribute['Report_Date_String'] = col
            confirms['features'].append({'attributes': attribute})

with open(deathCsvPath) as deathCsv:
    deathCsvReader = csv.DictReader(deathCsv)
    cols = deathCsvReader.fieldnames
    for index, col in enumerate(cols, start=0):
        if (index > 3):
            date = datetime.datetime.strptime(col,"%m/%d/%y")
            timestamp = time.mktime(date.timetuple())*1000
            sum_total = 0
            death = {}
            for row_index, row in enumerate(deathCsvReader, start=0):
                if (row_index > 0):
                    sum_total = sum_total + int(row[col])
            deathCsv.seek(0)
            # attribute['Report_Date'] = timestamp
            # attribute['Mainland_China'] = sum_china
            # attribute['Other_Locations'] = sum_world
            # attribute['Total_Confirmed'] = sum_total
            # attribute['Death'] = sum_china
            # attribute['Recovered'] = sum_world
            deaths[col] = sum_total

with open(recoveredCsvPath) as recoveredCsv:
    recoveredCsvReader = csv.DictReader(recoveredCsv)
    cols = recoveredCsvReader.fieldnames
    for index, col in enumerate(cols, start=0):
        if (index > 3):
            date = datetime.datetime.strptime(col,"%m/%d/%y")
            timestamp = time.mktime(date.timetuple())*1000
            sum_total = 0
            recovered = {}
            for row_index, row in enumerate(recoveredCsvReader, start=0):
                if (row_index > 0):
                    sum_total = sum_total + int(row[col])
            recoveredCsv.seek(0)
            # attribute['Report_Date'] = timestamp
            # attribute['Mainland_China'] = sum_china
            # attribute['Other_Locations'] = sum_world
            # attribute['Total_Confirmed'] = sum_total
            # attribute['Death'] = sum_china
            # attribute['Recovered'] = sum_world
            recovereds[col] = sum_total

for record in confirms['features']:
#add death
    if (record['attributes']['Report_Date_String'] in deaths):
        record['attributes']['Death'] = deaths[record['attributes']['Report_Date_String']]
    else:
        record['attributes']['Death'] = 0

    if (record['attributes']['Report_Date_String'] in recovereds):
        record['attributes']['Recovered'] = recovereds[record['attributes']['Report_Date_String']]
    else:
        record['attributes']['Recovered'] = 0

# print(confirms)
with open('_chart.json', 'w') as outfile:
    json.dump(confirms, outfile)
