import json

with open('_case.json') as json_file:
    data = json.load(json_file)
    for p in data['features']:
        if p['attributes']['Country_Region'] == "Others":
			p['attributes']['Country_Region'] = "Diamond Princess cruise ship"
        # print('Website: ' + p['website'])
        # print('From: ' + p['from'])
        #print(p['attributes']['Country_Region'])
    with open('_case_converted.json', 'w') as outfile:
    	json.dump(data, outfile)
