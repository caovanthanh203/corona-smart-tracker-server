import json

_from = "Saudi Arabia"
_to = "Saudi Arabia"

with open('_map.json') as json_file:
    data = json.load(json_file)
    for p in data['features']:
        p['attributes']['Country_Region'] = _to
        province_name = p['attributes']['Province_State']
        province_child_name = p['attributes']['Province_State_Child']
        name = ""
        if province_child_name is not None:
            name = province_child_name.capitalize() + ", "
        if province_name is not None:
            name = province_name.capitalize()
        p['attributes']['Province_State'] = name
        p['attributes']['Lat'] = p['geometry']['y']
        p['attributes']['Long_'] = p['geometry']['x']
    objs = data['features']
with open('_map_override.json') as json_file:
    output = json.load(json_file)
    for x in output['features']:
        if x['attributes']['Country_Region'] == _to:
            output['features'].remove(x)
    output["features"] =  output["features"] + objs
with open('_map_overrided.json', 'w') as outfile:
   	json.dump(output, outfile)
