import json

output={'features':[]}
districts = []
details = {}
error=False
excluded=[
	"Unknown",
	"Evacuees",
	"EVACUEES",
	"Gujarat*",
	"Italians",
	"Italians*",
	"Other Region*",
	"Tirupathur",
	"Other States",
	"Evacuees*",
	"Other Region",
	"Other States*"
]
with open('districts.json') as districts_file:
	districts = json.load(districts_file)
with open('_map_temp.json') as json_file:
    data = json.load(json_file)
    for state in data:
    	if state != "Unknown":
    		for district in data[state]["districtData"]:
    			if district not in excluded:
		    		if (district not in districts):
		    			print(district)