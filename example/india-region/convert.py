import json

output={'features':[]}
districts = []
details = {}
error=False
excluded=[
	"Unknown",
	"Evacuees",
	"EVACUEES",
	"Gujarat*",
	"Italians",
	"Italians*",
	"Other Region*",
	"Tirupathur",
	"Other States",
	"Evacuees*",
	"Other Region",
	"Other States*"
]
with open('districts.json') as districts_file:
	districts = json.load(districts_file)
with open('details.json') as details_file:
	details = json.load(details_file)
with open('_map_temp.json') as json_file:
    data = json.load(json_file)
    for state in data:
    	if state != "Unknown":
    		for district in data[state]["districtData"]:
    			if district not in excluded:
		    		if (district in districts):
		    			full_name = district + ", " + state
		    			output["features"].append({
		    				'attributes': {
								'Province_State': full_name,
								'Country_Region': 'India',
			                    'Confirmed': data[state]["districtData"][district]['confirmed'],
	        		            'Deaths': -1,
	                        	'Recovered': -1,
	                            'Lat': details[district]['Lat'],
	   			                'Long_': details[district]['Long']
	                        	}
	                        }
	                    )
		    		else:
			    		error=True
if (error):
	with open('hasErr', 'w') as errFile:
		json.dump({"error": error}, errFile)
with open('_map.json', 'w') as outfile:
    json.dump(output, outfile)
