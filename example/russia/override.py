import json

_from = "Russia"
_to = "Russia"

with open('_map.json') as json_file:
    data = json.load(json_file)
    objs = data

with open('_map_override.json') as json_file:
    output = json.load(json_file)
    for x in output['features']:
        if x['attributes']['Country_Region'] == _to:
            output['features'].remove(x)
    output["features"] =  output["features"] + objs["features"]

with open('_map_overrided.json', 'w') as outfile:
   	json.dump(output, outfile)
