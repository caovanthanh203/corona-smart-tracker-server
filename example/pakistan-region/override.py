# -*- coding: utf-8 -*-
import json

_to = u"Pakistan"
with open('_map.json') as json_file:
    data = json.load(json_file)
    objs = data['features']
with open('_map_override.json') as json_file:
    output = json.load(json_file)
    index = 0
    selected = []
    for records in output['features']:
        if unicode(records['attributes']['Country_Region']) !=  unicode(_to):
            selected.append(records)
    # print(selected)
    output["features"] =  selected + objs
with open('_map_overrided.json', 'w') as outfile:
   	json.dump(output, outfile)
