# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'world-meter-corona'
    start_urls = [
        'https://www.worldometers.info/coronavirus/'
    ]

    excluded=[
        "All",
        "Europe",
        "North America",
        "Asia",
        "South America",
        "Africa",
        "Oceania",
        "World",
        ""
    ]

    def parse(self, response):
    	# content = response.xpath('//table[@class="wikitable plainrowheaders sortable"]/tbody').extract()
    	# print(content)
        features = {"features": []}
    	# 
        for quote in response.xpath('//table[@id="main_table_countries_today"]/tbody[1]/tr'):
        	# print(''.join(quote.xpath('./td[2]/text()').re('[0-9]')))
        	# s = quote.xpath('./td[1]/text()').extract_first().strip().replace(',', "")
        	# print(s.strip().replace(',', ""))
            country_name = ''.join(quote.xpath('./td[2]//text()').extract()).strip()
            if (country_name not in self.excluded):
                features["features"].append(
            	 	{
            	 		'attributes': {
                    		'Country_Region': country_name,
                            'Confirmed': int("0" + ''.join(quote.xpath('./td[3]/text()').re('[0-9]'))),
                     		'Deaths': int("0" + ''.join(quote.xpath('./td[5]/text()').re('[0-9]'))),
                     		'Recovered': int("0" + ''.join(quote.xpath('./td[7]/text()').re('[0-9]')))
                     	}
                     }
                 )
        # #         'data-category': quote.xpath('./@data-category').extract_first()
        # # # # #         # 'author': quote.xpath('.//small[@class="author"]/text()').extract_first(),
        # # # # #         # 'tags': quote.xpath('.//div[@class="tags"]/a[@class="tag"]/text()').extract()
        with open('_case_main.json', 'w') as outfile:
    	    json.dump(features, outfile)
        # yield {
        # 	"features": features["features"]
        # }

        # next_page_url = response.xpath('//a[@class="next"]/@href').extract_first()
        # print(next_page_url)
        # if next_page_url is not None:
        #     yield scrapy.Request(response.urljoin(next_page_url))

