# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-south-africa-wiki'
    start_urls = [
        'https://en.wikipedia.org/wiki/Template:2019%E2%80%9320_coronavirus_pandemic_data/South_Africa_medical_cases'
    ]

    provinces = [
        "Eastern Cape",
        "Free State",
        "Gauteng",
        "KwaZulu-Natal",
        "Limpopo",
        "Mpumalanga",
        "North West",
        "Northern Cape",
        "Western Cape"
    ]

    def parse(self, response):

        features = {"features": []}
        rows = response.xpath('//*[@id="mw-content-text"]/div/table/tbody//tr')
        # print(rows)
        index = 0
        length = len(rows)
        confirm_row = rows[length - 2]
        # state_row = rows[length - 1]
        confirm_col = confirm_row.xpath('./td')
        for col_index in range(1, 10):
            col = confirm_col[col_index]
            # print(col.xpath('.//@title').extract_first())
            # if (index == length - 2):
            provine_name = self.provinces[index]
            # print(provine_name)
            confirmed = int("0" + ''.join(col.xpath('.//text()').re('[0-9]')))
            # print(confirmed)
            death = -1
            recovered = -1
            features={
                'attributes': {
                        'Province_State': provine_name,
                        'Country_Region': "South Africa",
                        'Confirmed': confirmed,
                        'Deaths': death,
                        'Recovered': recovered
                    }
                }
            index = index + 1
            yield features