# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-vietnam-wiki'
    start_urls = [
        'https://vi.wikipedia.org/wiki/B%E1%BA%A3n_m%E1%BA%ABu:S%E1%BB%91_ca_nhi%E1%BB%85m_COVID-19_theo_t%E1%BB%89nh_th%C3%A0nh_Vi%E1%BB%87t_Nam'
    ]

    def parse(self, response):

        # provindes = [
        #     "Grand Est",
        #     "Île-de-France",
        #     "Auvergne-Rhône-Alpes",
        #     "Hauts-de-France",
        #     "Burgondy-Franche-Comté",
        #     "Provence-Alpes-Côte d'Azur",
        #     "Occitania",
        #     "Brittany",
        #     "New Aquitaine",
        #     "Normandy",
        #     "Corse",
        #     "Pays de la Loire",
        #     "Centre-Val de Loire",
        #     "French Guiana",
        #     "Martinique",
        #     "Saint Martin",
        #     "Saint Barthélemy",
        #     "French Polynesia"
        # ]

        # provide_details = [
        #     {
        #         'Lat': 48.7544,
        #         'Long_': 5.8517
        #     },
        #     {
        #         'Lat': 48.5,
        #         'Long_': 2.5
        #     },
        #     {
        #         'Lat': 45.4471,
        #         'Long_': 4.3853
        #     },
        #     {
        #         'Lat': 49.9206,
        #         'Long_': 2.703
        #     },
        #     {
        #         'Lat': 47.2805,
        #         'Long_': 4.9994
        #     },
        #     {
        #         'Lat': 43.9352,
        #         'Long_': 6.0679
        #     },
        #     {
        #         'Lat': 43.8927,
        #         'Long_': 3.2828
        #     },
        #     {
        #         'Lat': 48.2020,
        #         'Long_': -2.9326
        #     },
        #     {
        #         'Lat': 45.7087,
        #         'Long_': 0.6269
        #     },
        #     {
        #         'Lat': 48.8799,
        #         'Long_': 0.1713
        #     },
        #     {
        #         'Lat': 42.0396,
        #         'Long_': 9.0129
        #     },
        #     {
        #         'Lat': 47.7633,
        #         'Long_': -0.3300
        #     },
        #     {
        #         'Lat': 47.7516,
        #         'Long_': 1.6751
        #     },
        #     {
        #         'Lat': 3.9339,
        #         'Long_': -53.1258
        #     },
        #     {
        #         'Lat': 14.6415,
        #         'Long_': -61.0242
        #     },
        #     {
        #         'Lat': 18.0708,
        #         'Long_': -63.0501
        #     },
        #     {
        #         'Lat': 17.9000,
        #         'Long_': -62.8333
        #     },
        #     {
        #         'Lat': -17.6797,
        #         'Long_': -149.4068
        #     }
        # ]
        # 

        features = {"features": []}
        rows = response.xpath('//*[@id="mw-content-text"]/div/table/tbody//tr')
        # print(rows)
        index = 0
        length = len(rows)
        for row in rows:
            if (index < length -1):
                provine_name = ''.join(row.xpath('.//td[1]//text()').re('[^\n]'))
                provine_url = ''.join(row.xpath('.//td[1]//@href').re('[^\n]'))
                # print(provine_name)
                url_nested = 'https://vi.wikipedia.org' + provine_url
                confirmed = int("0" + ''.join(row.xpath('.//td[2]//text()').re('[0-9]')))
                death = int("0" + ''.join(row.xpath('.//td[3]//text()').re('[0-9]')))
                recovered = int("0" + ''.join(row.xpath('.//td[4]//text()').re('[0-9]')))
                features={
                    'attributes': {
                            'Province_State': provine_name,
                            'Country_Region': u'Việt Nam',
                            'Confirmed': confirmed,
                            'Deaths': death,
                            'Recovered': recovered
                        }
                    }
                index = index + 1
                yield features
            # yield scrapy.Request(url=url_nested, meta={'features':features}, callback=self.parse_netsted_item)
            # print(provine_name)
        #     if provine_name in provindes:
        # # rows = response.xpath('//table[2]/tbody/tr')
        # # title_cols = title_row[len(title_row)-2].xpath('./th')
        # # cols = rows[len(rows)-2].xpath('./td')
        # # for col_index in range(1, 32):
        # #     provide = title_cols[col_index-1].xpath('.//@title')
        #         death = int("0" + ''.join(row.xpath('.//td[3]//text()').re('[0-9]')))
        #         recovered = int("0" + ''.join(row.xpath('.//td[4]//text()').re('[0-9]')))
        # #     if len(provide) == 0:
        # #         provide = title_cols[col_index-1].xpath('.//text()')
        # #     provide = ''.join(provide.re('[^\n]'))
        # #     # print(provide)
        # #     # print(s.strip().replace(',', ""))
        #         index = index + 1
        #         features["features"].append({
        #             'attributes': {
        #                     'Province_State': provine_name,
        #                     'Country_Region': 'France',
        #                     'Confirmed': confirmed,
        #                     'Deaths': death,
        #                     'Recovered': recovered,
        #                     'Lat': provide_details[index - 1]['Lat'],
        #                     'Long_': provide_details[index - 1]['Long_']
        #                 }
        #             }
        #         )
        # with open('_map.json', 'w') as outfile:
        #     json.dump(features, outfile)
        #print(''.join(cols[col_index].xpath('.//text()').re('[0-9]')))
        # # 
     #    for quote in response.xpath('//table[@class="wikitable plainrowheaders sortable mw-collapsible"]/tbody/tr[not(@class)]')[2:]:
     #      # print(''.join(quote.xpath('./td[1]/text()').re('[0-9]')))
     #      # s = quote.xpath('./td[1]/text()').extract_first().strip().replace(',', "")
     #      # print(s.strip().replace(',', ""))
     #      features["features"].append(
     #          {
     #              'attributes': {
     #                  'Country_Region': ''.join(quote.xpath('./th//text()').re('^[a-zA-Z()].*')),
     #                    'Confirmed': ''.join(quote.xpath('./td[1]/text()').re('[0-9]')),
     #                  'Deaths': ''.join(quote.xpath('./td[2]/text()').re('[0-9]')),
     #                  'Recovered': ''.join(quote.xpath('./td[3]/text()').re('[0-9]'))
     #              }
     #             }
     #         )
     #    # #         'data-category': quote.xpath('./@data-category').extract_first()
     #    # # # # #         # 'author': quote.sxpath('.//small[@class="author"]/text()').extract_first(),
     #    # # # # #         # 'tags': quote.xpath('.//div[@class="tags"]/a[@class="tag"]/text()').extract()
     #    with open('_case.json', 'w') as outfile:
        #     json.dump(features, outfile)
        # yield {
        #   "features": features["features"]
        # }

        # next_page_url = response.xpath('//a[@class="next"]/@href').extract_first()
        # print(next_page_url)
        # if next_page_url is not None:
        #     yield scrapy.Request(response.urljoin(next_page_url))
    # def parse_netsted_item(self, response):
    #     # print(response.meta.get('provine_name'))
    #     features = response.meta.get('features')
    #     # print(features)
    #     geo = response.xpath('//span[@class="geo-nondefault"]//span[@class="geo"]//text()').extract_first()
    #     if geo is not None:
    #         geo = geo.split(';')
    #         # print(geo[0].strip())
    #         # print(geo[1].strip())
    #         features["attributes"]["Lat"]=geo[0].strip()
    #         features["attributes"]["Long_"]=geo[1].strip()
    #         yield(features)
# Quảng Nam
# Quảng Ninh
# Thanh Hóa
# Ninh Thuận
# Ninh Bình
# Lào Cai
# Bình Thuận
# Khánh Hòa
# Đà Nẵng
# Vĩnh Phúc
# Hà Nội
# Thừa Thiên Huế
# TP. Hồ Chí Minh

# Hà Nội  14  0   0
# Vĩnh Phúc   11  0   11
# Bình Thuận  9   0   0
# TP. Hồ Chí Minh 8   0   3
# Quảng Ninh  5   0   0
# Quảng Nam   3   0   0
# Đà Nẵng 3   0   0
# Thừa Thiên Huế  2   0   0
# Lào Cai 2   0   0
# Ninh Thuận  1   0   0
# Ninh Bình   1   0   0
# Khánh Hòa   1   0   1
# Thanh Hóa

# <span class="geo">15.556898; 108.03680</span>
# <span class="geo">21.250982; 107.193604</span>
# <span class="geo">22.379997; 104.15786</span>
# <span class="geo">11.56556; 108.99028</span>
# <span class="geo">20.141049; 105.309448</span>
# <span class="geo">12.073058; 109.047768</span>
# <span class="geo">20.250924; 105.974808</span>
# <span class="geo">11.100252; 108.141174</span>
# <span class="geo">16.031944; 108.220556</span>
# <span class="geo">21.363571; 105.548401</span>
# <span class="geo">21.028333; 105.853333</span>
# <span class="geo">16.463744; 107.58482</span>
# <span class="geo">10.769444; 106.681944</span>
