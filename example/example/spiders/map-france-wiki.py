# -*- coding: utf-8 -*-
# @Author: Cao Van Thanh
# @Date:   2020-03-16 10:15:54
# @Last Modified by:   Cao Van Thanh
# @Last Modified time: 2020-03-16 11:40:14
# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-france-wiki'
    start_urls = [
        'https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_France'
    ]

    def parse(self, response):

        provindes = [
            u"Grand Est",
            u"Île-de-France",
            u"Auvergne-Rhône-Alpes",
            u"Hauts-de-France",
            u"Burgondy-Franche-Comté",
            u"Provence-Alpes-Côte d'Azur",
            u"Occitania",
            u"Brittany",
            u"New Aquitaine",
            u"Normandy",
            u"Corse",
            u"Pays de la Loire",
            u"Centre-Val de Loire",
            u"French Guiana",
            u"Martinique",
            u"Saint Martin",
            u"Saint Barthélemy",
            u"French Polynesia"
        ]

        provide_details = [
            {
                'Lat': 48.7544,
                'Long_': 5.8517
            },
            {
                'Lat': 48.5,
                'Long_': 2.5
            },
            {
                'Lat': 45.4471,
                'Long_': 4.3853
            },
            {
                'Lat': 49.9206,
                'Long_': 2.703
            },
            {
                'Lat': 47.2805,
                'Long_': 4.9994
            },
            {
                'Lat': 43.9352,
                'Long_': 6.0679
            },
            {
                'Lat': 43.8927,
                'Long_': 3.2828
            },
            {
                'Lat': 48.2020,
                'Long_': -2.9326
            },
            {
                'Lat': 45.7087,
                'Long_': 0.6269
            },
            {
                'Lat': 48.8799,
                'Long_': 0.1713
            },
            {
                'Lat': 42.0396,
                'Long_': 9.0129
            },
            {
                'Lat': 47.7633,
                'Long_': -0.3300
            },
            {
                'Lat': 47.7516,
                'Long_': 1.6751
            },
            {
                'Lat': 3.9339,
                'Long_': -53.1258
            },
            {
                'Lat': 14.6415,
                'Long_': -61.0242
            },
            {
                'Lat': 18.0708,
                'Long_': -63.0501
            },
            {
                'Lat': 17.9000,
                'Long_': -62.8333
            },
            {
                'Lat': -17.6797,
                'Long_': -149.4068
            }
        ]

        features = {"features": []}

        rows = response.xpath('//table[5]//tr')
        index = 0
        for row in rows:
            provine_name = ''.join(row.xpath('.//td[1]//text()').re('[^\n]'))
            if provine_name in provindes:
                # print(provine_name)
        # rows = response.xpath('//table[2]/tbody/tr')
        # title_cols = title_row[len(title_row)-2].xpath('./th')
        # cols = rows[len(rows)-2].xpath('./td')
        # for col_index in range(1, 32):
        #     provide = title_cols[col_index-1].xpath('.//@title')
                confirmed = int("0" + ''.join(row.xpath('.//td[2]//text()').re('[0-9]')))
                death = int("0" + ''.join(row.xpath('.//td[3]//text()').re('[0-9]')))
                recovered = int("0" + ''.join(row.xpath('.//td[4]//text()').re('[0-9]')))
        #     if len(provide) == 0:
        #         provide = title_cols[col_index-1].xpath('.//text()')
        #     provide = ''.join(provide.re('[^\n]'))
        #     # print(provide)
        #     # print(s.strip().replace(',', ""))
                index = index + 1
                features["features"].append({
                    'attributes': {
                            'Province_State': provine_name,
                            'Country_Region': 'France',
                            'Confirmed': confirmed,
                            'Deaths': death,
                            'Recovered': recovered,
                            'Lat': provide_details[index - 1]['Lat'],
                            'Long_': provide_details[index - 1]['Long_']
                        }
                    }
                )
        with open('_map.json', 'w') as outfile:
            json.dump(features, outfile)
        #print(''.join(cols[col_index].xpath('.//text()').re('[0-9]')))
    	# # 
     #    for quote in response.xpath('//table[@class="wikitable plainrowheaders sortable mw-collapsible"]/tbody/tr[not(@class)]')[2:]:
     #    	# print(''.join(quote.xpath('./td[1]/text()').re('[0-9]')))
     #    	# s = quote.xpath('./td[1]/text()').extract_first().strip().replace(',', "")
     #    	# print(s.strip().replace(',', ""))
     #    	features["features"].append(
     #    	 	{
     #    	 		'attributes': {
     #            		'Country_Region': ''.join(quote.xpath('./th//text()').re('^[a-zA-Z()].*')),
     #                    'Confirmed': ''.join(quote.xpath('./td[1]/text()').re('[0-9]')),
     #             		'Deaths': ''.join(quote.xpath('./td[2]/text()').re('[0-9]')),
     #             		'Recovered': ''.join(quote.xpath('./td[3]/text()').re('[0-9]'))
     #             	}
     #             }
     #         )
     #    # #         'data-category': quote.xpath('./@data-category').extract_first()
     #    # # # # #         # 'author': quote.sxpath('.//small[@class="author"]/text()').extract_first(),
     #    # # # # #         # 'tags': quote.xpath('.//div[@class="tags"]/a[@class="tag"]/text()').extract()
     #    with open('_case.json', 'w') as outfile:
    	#     json.dump(features, outfile)
        # yield {
        # 	"features": features["features"]
        # }

        # next_page_url = response.xpath('//a[@class="next"]/@href').extract_first()
        # print(next_page_url)
        # if next_page_url is not None:
        #     yield scrapy.Request(response.urljoin(next_page_url))

