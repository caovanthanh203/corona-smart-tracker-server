# -*- coding: utf-8 -*-
import scrapy


class DacapSpider(scrapy.Spider):
    name = 'districts'
    start_urls = ['https://en.wikipedia.org/wiki/List_of_states_of_Mexico']

    def parse(self, response):

            features = {"features": []}
            index = 0

            table = response.xpath('//*[@id="mw-content-text"]/div/table')
            rows = table.xpath('./tbody/tr')
            for row in rows:
                # provine_index = row.xpath('./td[1]//text()').extract_first()
                state_name = row.xpath('./td[1]//text()').extract_first()
                if (state_name is not None):
                    # if (provine_index == "1"):
                    #     index = index + 1
                        # print(provine_index)
                    # province_name_full = provine_name + ", " + state_name
                    provine_url = ''.join(row.xpath('.//td[1]//@href').re('[^\n]'))
                    # print(provine_name)
                    url_nested = 'https://en.wikipedia.org' + provine_url
                    detail = {
                        'Province_State': state_name,
                        'Lat': 0,
                        # 'district': provine_name,
                        'Long': 0,
                    }
                    # if (provine_name in self.districts):
                    #     yield({provine_name})
                    yield scrapy.Request(url=url_nested, meta={'detail':detail}, callback=self.parse_netsted_item)

    def parse_netsted_item(self, response):
        # print(response.meta.get('provine_name'))
        detail = response.meta.get('detail')
        # print(province_name_full)
        # print(features)
        geos = response.xpath('//span[@class="geo"]//text()').extract_first()
        if geos is not None:
            geo = geos.split(';')
            # print(geo)
            # coord = geo[0].split(' ')
            detail['Lat'] = geo[0].strip()
            detail['Long'] = geo[1].strip()

            # print(coord)
            print("\033[42m"+detail['Province_State']+"\033[0m")
            yield({detail['Province_State']: detail})
        else:
            # print(geo)
            print("\033[41m"+detail['Province_State']+"\033[0m")
            # yield({'name': province_name_full})
        #     # print(geo[0].strip())
            # print(geo[1].strip())
            # features["attributes"]["Lat"]=geo[0].strip()
            # features["attributes"]["Long_"]=geo[1].strip()
            # yield(detail)
            # print