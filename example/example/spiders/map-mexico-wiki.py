# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-mexico-wiki'
    start_urls = [
        'https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Mexico'
    ]

    def parse(self, response):

        features = {"features": []}
        rows = response.xpath('//*[@id="mw-content-text"]/div/table[3]/tbody//tr')
        # print(rows)
        index = 0
        for row in rows:
            if index > 1:
                provine_name = ''.join(row.xpath('.//th[1]//text()').re('[^\n]')).strip()
                # print(provine_name)
                confirmed = int("0" + ''.join(row.xpath('.//td[1]//text()').re('[0-9]')))
                death = int("0" + ''.join(row.xpath('.//td[2]//text()').re('[0-9]')))
                recovered = int("0" + ''.join(row.xpath('.//td[3]//text()').re('[0-9]')))
                features={
                    'attributes': {
                            'Province_State': provine_name,
                            'Country_Region': 'Mexicos',
                            'Confirmed': confirmed,
                            'Deaths': death,
                            'Recovered': recovered
                        }
                    }
                yield features
            index = index + 1
