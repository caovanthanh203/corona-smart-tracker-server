# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-pakistan-wiki'
    start_urls = [
        'http://covid.gov.pk/'
    ]

    provinces = [
        "ICT",
        "Punjab",
        "Sindh",
        "KP",
        "Balochistan",
        "AJK",
        "GB"
    ]

    def parse(self, response):

        features = {"features": []}
        rows = response.xpath('//*[@id="statistics"]/div[@class="row provinc-stat"]/div[@class="col"]')
        # print(rows)
        # pass
        index = 0
        # length = len(rows)
        # confirm_row = rows[length - 2]
        # state_row = rows[length - 1]
        # confirm_col = confirm_row.xpath('./td')
        for row in rows:
            province_name = row.xpath('./h6//text()').extract_first()
            if province_name in self.provinces:
                # col = confirm_col[col_index]
                # print(col.xpath('.//@title').extract_first())
                # if (index == length - 2):
                # provine_name = self.provinces[index]
                # # print(provine_name)
                confirmed = int("0" + ''.join(row.xpath('./h4//text()').re('[0-9]')))
                # print(confirmed)
                death = -1
                recovered = -1
                features={
                    'attributes': {
                            'Province_State': province_name,
                            'Country_Region': "Pakistan",
                            'Confirmed': confirmed,
                            'Deaths': death,
                            'Recovered': recovered
                        }
                    }
                # index = index + 1
                yield features