# -*- coding: utf-8 -*-
# @Author: Cao Van Thanh
# @Date:   2020-03-16 10:15:54
# @Last Modified by:   Cao Van Thanh
# @Last Modified time: 2020-04-09 19:38:51
# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-turkey-wiki'
    start_urls = [
        'https://en.wikipedia.org/wiki/Template:2019%E2%80%9320_coronavirus_pandemic_data/Turkey_medical_cases_by_province'
    ]

    provinces = [
        u"Istanbul",
        u"İzmir",
        u"Ankara",
        u"Konya",
        u"Kocaeli",
        u"Sakarya",
        u"Isparta",
        u"Bursa",
        u"Adana",
        u"Zonguldak",
        u"Samsun",
        u"Kayseri",
        u"Tekirdağ",
        u"Eskişehir",
        u"Balıkesir",
        u"Antalya",
        u"Rize",
        u"Manisa",
        u"Edirne",
        u"Tokat",
        u"Ordu",
        u"Trabzon",
        u"Denizli",
        u"Şırnak",
        u"Erzurum",
        u"Giresun",
        u"Malatya",
        u"Yalova",
        u"Mardin",
        u"Gaziantep",
        u"Kırklareli",
        u"Osmaniye",
        u"Diyarbakır",
        u"Muğla",
        u"Siirt",
        u"Uşak",
        u"Amasya",
        u"Çorum",
        u"Sinop",
        u"Adıyaman",
        u"Düzce",
        u"Hatay",
        u"Ağrı",
        u"Çanakkale",
        u"Kahramanmaraş",
        u"Nevşehir",
        u"Iğdır",
        u"Kastamonu",
        u"Çankırı",
        u"Van",
        u"Bayburt",
        u"Kırıkkale",
        u"Bitlis",
        u"Artvin",
        u"Aydın",
        u"Karabük",
        u"Bolu",
        u"Afyonkarahisar",
        u"Kars",
        u"Şanlıurfa",
        u"Kilis",
        u"Mersin",
        u"Bilecik",
        u"Erzincan",
        u"Yozgat",
        u"Karaman",
        u"Muş",
        u"Elazığ",
        u"Gümüşhane",
        u"Niğde",
        u"Bingöl",
        u"Bartın",
        u"Batman",
        u"Sivas",
        u"Kırşehir",
        u"Tunceli",
        u"Aksaray",
        u"Ardahan",
        u"Kütahya",
        u"Burdur",
        u"Hakkâri",
        ]

    def parse(self, response):
        features = {"features": []}
        rows = response.xpath('//*[@id="covid19-container"]/table/tbody/tr')
        index = 0
        # print(rows)
        for row in rows:
            province_name = row.xpath('.//a/text()').extract_first()
            if (province_name in self.provinces):
                # print(province_name)
                provine_url = row.xpath('.//a/@href').extract_first()
                url_nested = 'https://en.wikipedia.org' + provine_url
                confirmed = int("0" + ''.join(row.xpath('.//td[1]//text()').re('[0-9]')))
                death = int("0" + ''.join(row.xpath('.//td[2]//text()').re('[0-9]')))
                recovered = -1# int("0" + ''.join(row.xpath('.//td[2]//text()').re('[0-9]')))
                features={
                    'attributes': {
                        'Province_State': province_name,
                        'Country_Region': "Turkey",
                        'Confirmed': confirmed,
                        'Deaths': death,
                        'Recovered': recovered
                    }
                }
                # print(features)
                yield scrapy.Request(url=url_nested, meta={'features':features}, callback=self.parse_netsted_item)

    def parse_netsted_item(self, response):
        features = response.meta.get('features')
        geo = response.xpath('//span[@class="geo-nondefault"]//span[@class="geo"]//text()').extract_first()
        if geo is not None:
            geo = geo.split(';')
            features["attributes"]["Lat"]=geo[0].strip()
            features["attributes"]["Long_"]=geo[1].strip()
            # print(features)
            yield(features)