# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-russia-wiki'
    start_urls = [
        'https://en.wikipedia.org/wiki/Template:2019%E2%80%9320_coronavirus_pandemic_data/Russia_medical_cases'
    ]

    provinces = [
			u"Moscow",
			u"Moscow Oblast",
			u"Saint Petersburg",
			u"Sverdlovsk Oblast",
			u"Komi Republic",
			u"Krasnodar Krai",
			u"Republic of Crimea",
			u"Yakutia",
			u"Chelyabinsk Oblast",
			u"Tatarstan",
			u"Samara Oblast",
			u"Krasnoyarsk Krai",
			u"Perm Krai",
			u"Buryatia",
			u"Tula Oblast",
			u"Kirov Oblast",
			u"Kaliningrad Oblast",
			u"Leningrad Oblast",
			u"Nizhny Novgorod Oblast",
			u"Lipetsk Oblast",
			u"Chechnya",
			u"Ivanovo Oblast",
			u"Novosibirsk Oblast",
			u"Pskov Oblast",
			u"Khabarovsk Krai",
			u"Arkhangelsk Oblast",
			u"Penza Oblast",
			u"Orenburg Oblast",
			u"Kabardino-Balkaria",
			u"Stavropol Krai",
			u"Voronezh Oblast",
			u"Bashkortostan",
			u"Khanty-Mansi Autonomous Okrug",
			u"Sevastopol",
			u"Ryazan Oblast",
			u"Murmansk Oblast",
			u"Adygea",
			u"Dagestan",
			u"Udmurtia",
			u"Belgorod Oblast",
			u"Sakhalin Oblast",
			u"Saratov Oblast",
			u"Volgograd Oblast",
			u"Tyumen Oblast",
			u"Tver Oblast",
			u"Khakassia",
			u"Mordovia",
			u"Bryansk Oblast",
			u"Kaluga Oblast",
			u"Novgorod Oblast",
			u"Omsk Oblast",
			u"Oryol Oblast",
			u"Tambov Oblast",
			u"Yaroslavl Oblast",
			u"Tomsk Oblast",
			u"Kemerovo Oblast",
			u"Mari El",
			u"Primorsky Krai",
			u"Irkutsk Oblast",
			u"Rostov Oblast",
			u"Vladimir Oblast",
			u"Vologda Oblast",
			u"Chuvashia",
			u"Kalmykia",
			u"Altai Krai",
			u"Amur Oblast",
			u"Kostroma Oblast",
			u"Kurgan Oblast",
			u"Smolensk Oblast",
			u"Ulyanovsk Oblast",
			u"Zabaykalsky Krai",
        ]

    def parse(self, response):
        features = {"features": []}
        rows = response.xpath('//*[@id="mw-content-text"]/div/div[4]/table[1]/tbody/tr')
        index = 0
        for row in rows:
            province_name = row.xpath('.//a/@title').extract_first()
            if (province_name in self.provinces):
            	# print(province_name)
            	provine_url = row.xpath('.//a/@href').extract_first()
            	url_nested = 'https://en.wikipedia.org' + provine_url
            	confirmed = int("0" + ''.join(row.xpath('.//td[1]//text()').re('[0-9]')))
            	death = int("0" + ''.join(row.xpath('.//td[3]//text()').re('[0-9]')))
            	recovered = int("0" + ''.join(row.xpath('.//td[2]//text()').re('[0-9]')))
            	features={
                    'attributes': {
                        'Province_State': province_name,
                        'Country_Region': "Russia",
                        'Confirmed': confirmed,
                        'Deaths': death,
                        'Recovered': recovered
                    }
                }
                yield scrapy.Request(url=url_nested, meta={'features':features}, callback=self.parse_netsted_item)

    def parse_netsted_item(self, response):
        features = response.meta.get('features')
        geo = response.xpath('//span[@class="geo-nondefault"]//span[@class="geo"]//text()').extract_first()
        if geo is not None:
            geo = geo.split(';')
            features["attributes"]["Lat"]=geo[0].strip()
            features["attributes"]["Long_"]=geo[1].strip()
            yield(features)