# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-iran-wiki'
    start_urls = [
        'https://en.wikipedia.org/wiki/Template:2019%E2%80%9320_coronavirus_pandemic_data/Iran_medical_cases'
    ]

    def parse(self, response):
        provide_details = [
            { 
                'Lat': 34.6401,   
                'Long_': 50.8764
            },
            { 
                'Lat': 35.69439,  
                'Long_': 51.42151
            },
            { 
                'Lat': 36.315482, 
                'Long_': 52.517056
            },
            { 
                'Lat': 35.48,     
                'Long_': 50.58
            },
            { 
                'Lat': 35.432351, 
                'Long_': 54.723075
            },
            { 
                'Lat': 37.26142,  
                'Long_': 55.18126
            },
            { 
                'Lat': 35.994224, 
                'Long_': 50.903877
            },
            { 
                'Lat': 32.65722,  
                'Long_': 51.67761
            },
            { 
                'Lat': 28.957136, 
                'Long_': 53.222766
            },
            { 
                'Lat': 26.307505, 
                'Long_': 57.852122
            },
            { 
                'Lat': 30.618442, 
                'Long_': 51.538184
            },
            { 
                'Lat': 31.919882, 
                'Long_': 50.818164
            },
            { 
                'Lat': 28.665254, 
                'Long_': 51.53051
            },
            { 
                'Lat': 37.023054, 
                'Long_': 49.663809
            },
            { 
                'Lat': 38.231638, 
                'Long_': 48.227679
            },
            { 
                'Lat': 37.994227, 
                'Long_': 46.476048
            },
            { 
                'Lat': 38.791327, 
                'Long_': 44.783185
            },
            { 
                'Lat': 35.610523, 
                'Long_': 47.007743
            },
            { 
                'Lat': 36.599989, 
                'Long_': 48.402154
            },
            { 
                'Lat': 34.273835, 
                'Long_': 49.663993
            },
            { 
                'Lat': 34.79922,  
                'Long_': 48.51456
            },
            { 
                'Lat': 31.3273,   
                'Long_': 48.694
            },
            { 
                'Lat': 34.31417,  
                'Long_': 47.065
            },
            { 
                'Lat': 33.487548, 
                'Long_': 48.459986
            },
            { 
                'Lat': 33.099535, 
                'Long_': 46.840458
            },
            { 
                'Lat': 35.570373, 
                'Long_': 59.099818
            },
            { 
                'Lat': 27.388036, 
                'Long_': 60.763203
            },
            { 
                'Lat': 31.89722,  
                'Long_': 54.3675
            },
            { 
                'Lat': 32.869994, 
                'Long_': 58.371785
            },
            { 
                'Lat': 30.28321,  
                'Long_': 57.07879
            },
            { 
                'Lat': 37.53283,  
                'Long_': 56.861297
            }
        ]
        title_row = response.xpath('//table[2]/tbody/tr[2]')
        rows = response.xpath('//table[2]/tbody/tr')
        title_cols = title_row[len(title_row)-2].xpath('./th')
        cols = rows[len(rows)-2].xpath('./td')
        features = {"features": []}
        for col_index in range(1, 32):
            provide = title_cols[col_index-1].xpath('.//@title')
            confirmed = int("0" + ''.join(cols[col_index].xpath('.//text()').re('[0-9]')))
            if len(provide) == 0:
                provide = title_cols[col_index-1].xpath('.//text()')
            provide = ''.join(provide.re('[^\n]'))
            # print(provide)
            # print(s.strip().replace(',', ""))
            features["features"].append({
                'attributes': {
                        'Province_State': provide,
                        'Country_Region': 'Iran',
                        'Confirmed': confirmed,
                        'Deaths': -1,
                        'Recovered': -1,
                        'Lat': provide_details[col_index - 1]['Lat'],
                        'Long_': provide_details[col_index - 1]['Long_']
                    }
                }
            )
        with open('_map.json', 'w') as outfile:
            json.dump(features, outfile)
        #print(''.join(cols[col_index].xpath('.//text()').re('[0-9]')))
    	# # 
     #    for quote in response.xpath('//table[@class="wikitable plainrowheaders sortable mw-collapsible"]/tbody/tr[not(@class)]')[2:]:
     #    	# print(''.join(quote.xpath('./td[1]/text()').re('[0-9]')))
     #    	# s = quote.xpath('./td[1]/text()').extract_first().strip().replace(',', "")
     #    	# print(s.strip().replace(',', ""))
     #    	features["features"].append(
     #    	 	{
     #    	 		'attributes': {
     #            		'Country_Region': ''.join(quote.xpath('./th//text()').re('^[a-zA-Z()].*')),
     #                    'Confirmed': ''.join(quote.xpath('./td[1]/text()').re('[0-9]')),
     #             		'Deaths': ''.join(quote.xpath('./td[2]/text()').re('[0-9]')),
     #             		'Recovered': ''.join(quote.xpath('./td[3]/text()').re('[0-9]'))
     #             	}
     #             }
     #         )
     #    # #         'data-category': quote.xpath('./@data-category').extract_first()
     #    # # # # #         # 'author': quote.sxpath('.//small[@class="author"]/text()').extract_first(),
     #    # # # # #         # 'tags': quote.xpath('.//div[@class="tags"]/a[@class="tag"]/text()').extract()
     #    with open('_case.json', 'w') as outfile:
    	#     json.dump(features, outfile)
        # yield {
        # 	"features": features["features"]
        # }

        # next_page_url = response.xpath('//a[@class="next"]/@href').extract_first()
        # print(next_page_url)
        # if next_page_url is not None:
        #     yield scrapy.Request(response.urljoin(next_page_url))

