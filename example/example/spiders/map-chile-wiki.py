# -*- coding: utf-8 -*-
# @Author: Cao Van Thanh
# @Date:   2020-03-16 10:15:54
# @Last Modified by:   Cao Van Thanh
# @Last Modified time: 2020-04-02 00:47:46
# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-chile-wiki'
    start_urls = [
        'https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Chile'
    ]

    provinces = [
			u"Arica y Parinacota",
			u"Tarapacá",
			u"Antofagasta",
			u"Atacama",
			u"Coquimbo",
			u"Valparaíso",
			u"Santiago Metropolitan Region",
			u"O'Higgins",
			u"Maule Region",
			u"Ñuble Region",
			u"Biobío Region",
			u"Araucanía Region",
			u"Los Ríos Region",
			u"Los Lagos Region",
			u"Aysén",
			u"Magallanes"
        ]

    provide_details = [
            {
            	'Lat': '-18.475',
            	'Long': '-70.314444'
            },
            {
            	'Lat': '-20.283333',
            	'Long': '-69.333333'
            },
            {
            	'Lat': '-23.644167',
            	'Long': '-70.410833'
            },
            {
            	'Lat': '-27.366667',
            	'Long': '-70.332222'
            },
            {
            	'Lat': '-29.907778',
            	'Long': '-70.254167'
            },
            {
            	'Lat': '-33.063056',
            	'Long': '-71.639444'
            },
            {
            	'Lat': '-33.437778',
            	'Long': '-70.650278'
            },
            {
            	'Lat': '-34.167222',
            	'Long': '-70.726944'
            },
            {
            	'Lat': '-35.433333',
            	'Long': '-71.666667'
            },
            {
            	'Lat': '-36.616667',
            	'Long': '-71.95'
            },
            {
            	'Lat': '-36.833333',
            	'Long': '-73.05'
            },
            {
            	'Lat': '-38.9',
            	'Long': '-72.666667'
            },
            {
            	'Lat': '-39.808333',
            	'Long': '-73.241667'
            },
            {
            	'Lat': '-41.471667',
            	'Long': '-72.936667'
            },
            {
            	'Lat': '-43.57',
            	'Long': '-72.066111'
            },
            {
            	'Lat': '-53.166667',
            	'Long': '-70.933333'
            }
        ]

    def parse(self, response):
        features = {"features": []}
        rows = response.xpath('//*[@id="mw-content-text"]/div/table[4]/tbody/tr')
        index = 0
        for row in rows:
            # print(row.xpath('.').extract_first())
            province_name = row.xpath('./td[1]//a/text()').extract_first()
            if (province_name in self.provinces):
            	# print(province_name)
            	provine_url = row.xpath('./td[1]//a/@href').extract_first()
            	url_nested = 'https://en.wikipedia.org' + provine_url
            	confirmed = int("0" + ''.join(row.xpath('.//td[2]//text()').re('[0-9]')))
            	death = int("0" + ''.join(row.xpath('.//td[3]//text()').re('[0-9]')))
            	recovered = -1
            	index = self.provinces.index(province_name)
            	features["features"].append({
                    'attributes': {
	                        'Province_State': province_name,
	                        'Country_Region': "Chile",
	                        'Confirmed': confirmed,
	                        'Deaths': death,
	                        'Recovered': recovered,
	                        'Lat': self.provide_details[index]['Lat'],
	                        'Long_': self.provide_details[index]['Long']
	                    }
	                }
                )
                with open('_map.json', 'w') as outfile:
		            json.dump(features, outfile)
                # yield scrapy.Request(url=url_nested, meta={'features':features}, callback=self.parse_netsted_item)

    # def parse_netsted_item(self, response):
    #     features = response.meta.get('features')
    #     geo = response.xpath('//span[@class="geo-nondefault"]//span[@class="geo"]//text()').extract_first()
    #     if geo is not None:
    #         geo = geo.split(';')
    #         features["attributes"]["Lat"]=geo[0].strip()
    #         features["attributes"]["Long_"]=geo[1].strip()
    #         yield(features)