# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'wiki-corona'
    start_urls = [
        'https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_outbreak_by_country_and_territory'
    ]

    def parse(self, response):
    	# content = response.xpath('//table[@class="wikitable plainrowheaders sortable"]/tbody').extract()
    	# print(content)
        features = {"features": []}
    	# 
        for quote in response.xpath('//table[@class="wikitable plainrowheaders sortable mw-collapsible"]/tbody/tr[not(@class)]')[2:]:
        	# print(''.join(quote.xpath('./td[1]/text()').re('[0-9]')))
        	# s = quote.xpath('./td[1]/text()').extract_first().strip().replace(',', "")
        	# print(s.strip().replace(',', ""))
        	features["features"].append(
        	 	{
        	 		'attributes': {
                		'Country_Region': ''.join(quote.xpath('./th//text()').re('^[a-zA-Z()].*')),
                                'Confirmed': ''.join(quote.xpath('./td[1]/text()').re('[0-9]')),
                 		'Deaths': ''.join(quote.xpath('./td[2]/text()').re('[0-9]')),
                 		'Recovered': ''.join(quote.xpath('./td[3]/text()').re('[0-9]'))
                 	}
                 }
             )
        # #         'data-category': quote.xpath('./@data-category').extract_first()
        # # # # #         # 'author': quote.xpath('.//small[@class="author"]/text()').extract_first(),
        # # # # #         # 'tags': quote.xpath('.//div[@class="tags"]/a[@class="tag"]/text()').extract()
        with open('_case.json', 'w') as outfile:
    	    json.dump(features, outfile)
        # yield {
        # 	"features": features["features"]
        # }

        # next_page_url = response.xpath('//a[@class="next"]/@href').extract_first()
        # print(next_page_url)
        # if next_page_url is not None:
        #     yield scrapy.Request(response.urljoin(next_page_url))

