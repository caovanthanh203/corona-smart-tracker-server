# -*- coding: utf-8 -*-
# @Author: Cao Van Thanh
# @Date:   2020-03-16 10:15:54
# @Last Modified by:   Cao Van Thanh
# @Last Modified time: 2020-03-24 11:55:40
# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-india-gov'
    start_urls = [
        'https://www.mohfw.gov.in'
    ]

    def parse(self, response):

        provindes = [
            u"Andhra Pradesh",
            u"Arunachal Pradesh",
            u"Assam",
            u"Bihar",
            u"Chhattisgarh",
            u"Goa",
            u"Gujarat",
            u"Haryana",
            u"Himachal Pradesh",
            u"Jharkhand",
            u"Karnataka",
            u"Kerala",
            u"Madhya Pradesh",
            u"Maharashtra",
            u"Manipur",
            u"Meghalaya",
            u"Mizoram",
            u"Nagaland",
            u"Odisha",
            u"Punjab",
            u"Rajasthan",
            u"Sikkim",
            u"Tamil Nadu",
            u"Telengana",
            u"Tripura",
            u"Uttar Pradesh",
            u"Uttarakhand",
            u"West Bengal",
            u"Andaman and Nicobar Islands",
            u"Chandigarh",
            u"Dadra and Nagar Haveli and Daman and Diu",
            u"Delhi",
            u"Jammu and Kashmir",
            u"Ladakh",
            u"Lakshadweep",
            u"Puducherry"
        ]

        provide_details = [
            {
                'Lat': 16.50,
                'Long_': 80.64
            },
            {
                'Lat': 27.06,
                'Long_': 93.37
            },
            {
                'Lat': 26.14,
                'Long_': 91.77
            },
            {
                'Lat': 25.4,
                'Long_': 85.1
            },
            {
                'Lat': 21.25,
                'Long_': 81.60
            },
            {
                'Lat': 15.50,
                'Long_': 73.83
            },
            {
                'Lat': 23.13,
                'Long_': 72.41
            },
            {
                'Lat': 30.44,
                'Long_': 76.47
            },
            {
                'Lat': 31.612,
                'Long_': 77.1020
            },
            {
                'Lat': 23.35,
                'Long_': 85.33
            },
            {
                'Lat': 12.97,
                'Long_': 77.50
            },
            {
                'Lat': 8.5,
                'Long_': 77
            },
            {
                'Lat': 23.25,
                'Long_': 77.417
            },
            {
                'Lat': 18.97,
                'Long_': 72.820
            },
            {
                'Lat': 24.6637,
                'Long_': 93.9063
            },
            {
                'Lat': 25.57,
                'Long_': 91.88
            },
            {
                'Lat': 23.36,
                'Long_': 92.8
            },
            {
                'Lat': 25.67,
                'Long_': 94.12
            },
            {
                'Lat': 20.27,
                'Long_': 85.82
            },
            {
                'Lat': 30.79,
                'Long_': 75.84
            },
            {
                'Lat': 26.6,
                'Long_': 73.8
            },
            {
                'Lat': 27.33,
                'Long_': 88.62
            },
            {
                'Lat': 13.09,
                'Long_': 80.27
            },
            {
                'Lat': 17.366,
                'Long_': 78.475
            },
            {
                'Lat': 23.84,
                'Long_': 91.28
            },
            {
                'Lat': 26.85,
                'Long_': 80.91
            },
            {
                'Lat': 30.33,
                'Long_': 78.06
            },
            {
                'Lat': 22.566667,
                'Long_': 88.366667
            },
            {
                'Lat': 11.68,
                'Long_': 92.77
            },
            {
                'Lat': 30.45,
                'Long_': 76.47
            },
            {
                'Lat': 20.42,
                'Long_': 72.83
            },
            {
                'Lat': 28.3636,
                'Long_': 77.1348
            },
            {
                'Lat': 33.7782,
                'Long_': 76.5762
            },
            {
                'Lat': 34.1012,
                'Long_': 77.3448
            },
            {
                'Lat': 10.6,
                'Long_': 72.6
            },
            {
                'Lat': 11.911082,
                'Long_': 79.812533
            }
        ]

        features = {"features": []}

        rows = response.xpath('//*[@id="cases"]//table/tbody/tr')
        for row in rows:
            provine_name = ''.join(row.xpath('.//td[2]//text()').re('[^\n]'))
            if provine_name in provindes:
                confirmed_india = int("0" + ''.join(row.xpath('.//td[3]//text()').re('[0-9]')))
                confirmed_foreign = int("0" + ''.join(row.xpath('.//td[4]//text()').re('[0-9]')))
                confirmed = confirmed_india + confirmed_foreign
                death = int("0" + ''.join(row.xpath('.//td[6]//text()').re('[0-9]')))
                recovered = int("0" + ''.join(row.xpath('.//td[5]//text()').re('[0-9]')))
                index = provindes.index(provine_name)
                features["features"].append({
                    'attributes': {
                            'Province_State': provine_name,
                            'Country_Region': 'India',
                            'Confirmed': confirmed,
                            'Deaths': death,
                            'Recovered': recovered,
                            'Lat': provide_details[index]['Lat'],
                            'Long_': provide_details[index]['Long_']
                        }
                    }
                )
        # print(features)
        # yield(features)
        with open('_map.json', 'w') as outfile:
            json.dump(features, outfile)
