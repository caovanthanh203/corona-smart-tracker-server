# -*- coding: utf-8 -*-
# @Author: Cao Van Thanh
# @Date:   2020-03-16 10:15:54
# @Last Modified by:   Cao Van Thanh
# @Last Modified time: 2020-04-02 02:10:00
# -*- coding: utf-8 -*-
import scrapy
import json

class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'map-morocco-wiki'
    start_urls = [
        'https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_in_Morocco'
    ]

    provinces = [
            u"Casablanca-Settat",
            u"Marrakech-Safi",
            u"Rabat-Salé-Kénitra",
            u"Fès-Meknès",
            u"Tanger-Tetouan-Al Hoceima",
            u"Oriental",
            u"Béni Mellal-Khénifra",
            u"Souss-Massa",
            u"Drâa-Tafilalet",
            u"Laâyoune-Sakia El Hamra",
            u"Guelmim-Oued Noun",
            u"Dakhla-Oued Ed-Dahab"
        ]

    provide_details = [
            {
                'Lat': '33.15',
                'Long': '-8.14'
            },
            {
                'Lat': '31.616667',
                'Long': '-8'
            },
            {
                'Lat': '34.033333',
                'Long': '-6.833333'
            },
            {
                'Lat': '34.033333',
                'Long': '-5'
            },
            {
                'Lat': '35.16',
                'Long': '-5.25'
            },
            {
                'Lat': '34.683333',
                'Long': '-1.9'
            },
            {
                'Lat': '32.46',
                'Long': '-6.35'
            },
            {
                'Lat': '30.08',
                'Long': '-8.48'
            },
            {
                'Lat': '31.32',
                'Long': '-5.33'
            },
            {
                'Lat': '26.133333',
                'Long': '-14.5'
            },
            {
                'Lat': '28.45',
                'Long': '-10.11'
            },
            {
                'Lat': '23',
                'Long': '-15'
            }
        ]

    def parse(self, response):
        features = {"features": []}
        rows = response.xpath('//*[@id="mw-content-text"]/div/table[3]/tbody/tr')
        index = 0
        for row in rows:
            # print(row.xpath('.').extract_first())
            province_name = row.xpath('./th[1]//a/text()').extract_first()
            if (province_name in self.provinces):
            	# print(province_name)
            	# provine_url = row.xpath('./td[1]//a/@href').extract_first()
            	# url_nested = 'https://en.wikipedia.org' + provine_url
            	confirmed = int("0" + ''.join(row.xpath('.//td[1]//text()').re('[0-9]')))
            	death = -1
            	recovered = -1
            	index = self.provinces.index(province_name)
            	features["features"].append({
                    'attributes': {
	                        'Province_State': province_name,
	                        'Country_Region': "Morocco",
	                        'Confirmed': confirmed,
	                        'Deaths': death,
	                        'Recovered': recovered,
	                        'Lat': self.provide_details[index]['Lat'],
	                        'Long_': self.provide_details[index]['Long']
	                    }
	                }
                )
                with open('_map.json', 'w') as outfile:
		            json.dump(features, outfile)
                # yield scrapy.Request(url=url_nested, meta={'features':features}, callback=self.parse_netsted_item)

    # def parse_netsted_item(self, response):
    #     features = response.meta.get('features')
    #     geo = response.xpath('//span[@class="geo-nondefault"]//span[@class="geo"]//text()').extract_first()
    #     if geo is not None:
    #         geo = geo.split(';')
    #         features["attributes"]["Lat"]=geo[0].strip()
    #         features["attributes"]["Long_"]=geo[1].strip()
    #         yield(features)