import json

_from = "Morocco"
_to = "Morocco"

with open('_map.json') as json_file:
    data = json.load(json_file)
    for p in data['features']:
        p['attributes']['Country_Region'] = _to
    objs = data['features']
with open('_map_override.json') as json_file:
    output = json.load(json_file)
    for x in output['features']:
        if x['attributes']['Country_Region'] == _to:
            output['features'].remove(x)
    output["features"] =  output["features"] + objs
with open('_map_overrided.json', 'w') as outfile:
    json.dump(output, outfile)