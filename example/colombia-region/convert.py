# -*- coding: utf-8 -*-
import json

output={'features':[]}
districts = []
details = {}
error=False
excluded=[
	"",
]
# with open('districts.json') as districts_file:
# 	districts = json.load(districts_file)
# with open('details.json') as details_file:
# 	details = json.load(details_file)
with open('_map_temp.json') as json_file:
    data = json.load(json_file)
    for district in data["data"][0]:
    	state_name = district[0]
    	confirmed = district[1]
    	geo = district[3].split(' ')
    	lat = geo[0]
    	_long = geo[1]
    	if state_name is not None:
			if state_name not in excluded:
				if (True or state_name in districts):
					# print(state_name)
					output["features"].append({
						'attributes': {
							'Country_Region': "Colombia",
							'Province_State': state_name,
							'Confirmed': confirmed,
							'Deaths': -1,
							'Recovered': -1,
	                        'Lat': lat,
				            'Long_': _long
					    	}
					    }
					)
				else:
					print(state_name)
					error=True
if (error):
	with open('hasErr', 'w') as errFile:
		json.dump({"error": error}, errFile)
with open('_map.json', 'w') as outfile:
    json.dump(output, outfile)
