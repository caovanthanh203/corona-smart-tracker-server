import json

output={'features':[]}
with open('_map_temp.json') as json_file:
    data = json.load(json_file)
    output['features'] = data

with open('_map.json', 'w') as outfile:
    json.dump(output, outfile)