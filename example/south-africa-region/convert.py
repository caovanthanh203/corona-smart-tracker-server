# -*- coding: utf-8 -*-
import json

output={'features':[]}
districts = []
details = {}
error=False
excluded=[
	"",
]
with open('districts.json') as districts_file:
	districts = json.load(districts_file)
with open('details.json') as details_file:
	details = json.load(details_file)
with open('_map_temp.json') as json_file:
    data = json.load(json_file)
    for district in data:
    	state_name = district['attributes']['Province_State']
    	if state_name is not None:
			if state_name not in excluded:
				if (state_name in districts):
					# print(state_name)
					output["features"].append({
						'attributes': {
							'Country_Region': u"South Africa",
							'Province_State': state_name,
							'Confirmed': district['attributes']['Confirmed'],
							'Deaths': district['attributes']['Deaths'],
							'Recovered': district['attributes']['Recovered'],
	                        'Lat': details[state_name]['Lat'],
				            'Long_': details[state_name]['Long']
					    	}
					    }
					)
				else:
					# print(state_name)
					error=True
if (error):
	with open('hasErr', 'w') as errFile:
		json.dump({"error": error}, errFile)
with open('_map.json', 'w') as outfile:
    json.dump(output, outfile)
