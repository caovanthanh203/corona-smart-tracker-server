# -*- coding: utf-8 -*-
import json

output={'features':[]}
districts = []
details = {}
error=False
excluded=[
	"",
]
with open('districts.json') as districts_file:
	districts = json.load(districts_file)
with open('_map_temp.json') as json_file:
    data = json.load(json_file)
    for district in data:
		state_name = district['attributes']['Province_State']
		if state_name is not None:
			if state_name not in excluded:
				if (state_name not in districts):
					print(state_name)