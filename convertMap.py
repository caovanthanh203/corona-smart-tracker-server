import json

with open('_map.json') as json_file:
    data = json.load(json_file)
    for p in data['features']:
        if p['attributes']['Province_State'] is not None:
			p['attributes']['Country_Region'] = p['attributes']['Province_State'] + ', ' + p['attributes']['Country_Region']
        # print('Website: ' + p['website'])
        # print('From: ' + p['from'])
        #print(p['attributes']['Country_Region'])
    with open('_map_converted.json', 'w') as outfile:
    	json.dump(data, outfile)
