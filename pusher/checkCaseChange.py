# -*- coding: utf-8 -*-
# @Author: Cao Van Thanh
# @Date:   2020-03-26 10:32:53
# @Last Modified by:   Cao Van Thanh
# @Last Modified time: 2020-03-28 17:33:03
import json
import requests
import pytz
import datetime
import re

new_data = {}
old_data = {}

def slug(message):
	message = message.replace(" ", "-")
	message = message.lower()
	message = re.sub(r"[^A-Za-z0-9-]", "", message)
	return message

def postToGroup(message_str):
	tz = pytz.timezone('Asia/Ho_Chi_Minh')
	x = datetime.datetime.now(tz)
	time_str = x.strftime("Việt Nam cập nhật lúc %H:%M ngày %d/%m/%Y")
	URL_PAGE="https://graph.facebook.com/192765545173455/feed"
	#URL_PAGE="https://graph.facebook.com/2625876387628506/feed"
	ACCESS_TOKEN="EAAJTNMXJQ88BABcaO3NfA4qN53XH8rYUeB0ozVaMXunYfKa5o24Ne1JuzUv82mqaiqytpScNs8xXd27DDXDJtO1rDDiZAvID8hY3XFKskzZCqZBamHQmYCIMEbqFVRdXxd4I6qbWOk7PUpJBCUgen4zWOdbZAb0d3daMKSisaEmm0omOTS0Nt4s9k20dXykZD"
	MESSAGE = time_str + message_str
	r = requests.post(URL_PAGE, data = {'message':MESSAGE, 'access_token':ACCESS_TOKEN})
	print(r.content)

def push(number, name, change, case):
	topic = slug(name)
	print('\tMessageId ' + str(number) + ' push to ' + topic)
	key = 'key=AAAAO28FcnQ:APA91bGp_gKVPAaD7MLPUQ2SuBzOJHsd__eDs4H9bQa56CfNrxOMUQ2mA6IdEHeyFRZCCr_WcNWkT9PCfgAip-Bs-LWunRrTRdhSkZ3ECNeaER7vZvrx4leHD2zZxaKS8Wtl_Lrr6qlT'
	url = 'https://fcm.googleapis.com/fcm/send'
	headers = {
		'Authorization': key,
		'Content-Type': 'application/json; UTF-8'
	}
	body = {
		'to':'/topics/' + topic,
		'data': {
			'title': '+' + change + ' case(s) in ' + name + '!',
			'body': case + ' case(s) confirmed.',
			'confirmed_change': change,
			'total_confirmed': case,
			'country_name': name,
			'message_number': number
		},
		"android":{
       			"ttl":"240s"
		}
	}
	r = requests.post(url, data=json.dumps(body), headers=headers)
	print('\t\t' + r.content)

with open('new.json') as newFile:
    newData = json.load(newFile)
    new_data = newData["features"]

with open('old.json') as oldFile:
    oldData = json.load(oldFile)
    old_data = oldData["features"]
number = 1
pushed = 0
for old_country in old_data:
	number = number + 1
	#print('Checking ' + slug(old_country["attributes"]["Country_Region"]))
	for new_country in new_data:
		if (old_country["attributes"]["Country_Region"] == new_country["attributes"]["Country_Region"]):
		    if (old_country["attributes"]["Confirmed"] < new_country["attributes"]["Confirmed"]):
			pushed = pushed + 1
		    	push(number, old_country["attributes"]["Country_Region"], str(new_country["attributes"]["Confirmed"] - old_country["attributes"]["Confirmed"]), str(new_country["attributes"]["Confirmed"]))
			#if (old_country["attributes"]["Country_Region"] == "Vietnam"):
		    		#delta = new_country["attributes"]["Confirmed"] - old_country["attributes"]["Confirmed"]
		    		#message_one = "\nXác nhận +" + str(delta) + " ca nhiễm mới"
		    		#message_two = "\nTổng ca nhiễm ghi nhận là " + str(new_country["attributes"]["Confirmed"])
		    		#message_three = "\n#VietNam #COVID19 #CoronaSmartTracker #bot"
		    		#postToGroup(message_one + message_two + message_three)
if (pushed > 0):
	print('Pushed ' + str(pushed) + ' messages\n')
else:
	print('No detected confirmed change, no push sent')
