const http = require('http');
const fs = require('fs');
const path = require('path');

const filename = 'chart.json';
const filePath = path.join(__dirname, filename);
const server = http.createServer((request, response) => {
  	fs.readFile(filePath, 'utf8', function (err, data) {
		if (err) throw err;
		try {
			obj = JSON.parse(data);
			response.writeHead(200, {"Content-Type": "application/json"});
			response.write(data);
			response.end();
		} catch (e) {
			console.error( e );
		}
	});
});

const port = process.env.PORT || 3002;
server.listen(port, (e) => console.log(e ? 'Oops': `Chart server running on http://localhost:${port}`));
