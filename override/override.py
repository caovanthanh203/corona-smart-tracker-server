import json

_from = "Vietnam"
_to = "Vietnam"
obj = {}

def extract_case(json):
    try:
        # Also convert to int since update_time will be string.  When comparing
        # strings, "10" is smaller than "2".
        # print(json["attributes"]['Confirmed'])
        return int(json["attributes"]['Confirmed'])
    except KeyError:
        return 0

with open('_case.json') as json_file:
    data = json.load(json_file)
    for p in data['features']:
        if p['attributes']['Country_Region'] == _from:
			obj = p['attributes']
        # print('Website: ' + p['website'])
        # print('From: ' + p['from'])
        #print(p['attributes']['Country_Region'])
with open('_case_override.json') as json_file:
    data = json.load(json_file)
    for p in data['features']:
        if p['attributes']['Country_Region'] == _to:
			p['attributes'] = obj
        # print('Website: ' + p['website'])
        # print('From: ' + p['from'])
        #print(p['attributes']['Country_Region'])

data["features"].sort(key=extract_case, reverse=True)
#message={"attributes": {"Confirmed": 0, "Country_Region": "Pls get v1.7 heatmap APK on XDA", "Recovered": 0, "Deaths": 0}}
#message2={"attributes": {"Confirmed": 0, "Country_Region": "Notify will be removed after 3h", "Recovered": 0, "Deaths": 0}}
#data["features"].insert(0, message2)
#data["features"].insert(0, message)
with open('_case_overrided.json', 'w') as outfile:
   	json.dump(data, outfile)
