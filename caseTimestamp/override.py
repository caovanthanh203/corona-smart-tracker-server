# -*- coding: utf-8 -*-
# @Author: Cao Van Thanh
# @Date:   2020-03-28 14:20:27
# @Last Modified by:   Cao Van Thanh
# @Last Modified time: 2020-03-28 17:00:02
# -*- coding: utf-8 -*-
# @Author: Cao Van Thanh
# @Date:   2020-03-26 10:32:53
# @Last Modified by:   Cao Van Thanh
# @Last Modified time: 2020-03-26 11:05:06
import json
import requests
import pytz
import time
import re

new_data = {}
old_data = {}

timestamp = time.time()

def slug(message):
        message = message.replace(" ", "-")
        message = message.lower()
        message = re.sub(r"[^A-Za-z0-9-]", "", message)
        return message

def confirm(json):
    try:
        # Also convert to int since update_time will be string.  When comparing
        # strings, "10" is smaller than "2".
        # print(json["attributes"]['Confirmed'])
        return int(json["attributes"]['Confirmed'])
    except KeyError:
        return 0

with open('_case.json') as newFile:
    newData = json.load(newFile)
    new_data = newData

with open('_case_old.json') as oldFile:
    oldData = json.load(oldFile)
    old_data = oldData

for old_country in old_data["features"]:
	#print(slug(old_country["attributes"]["Country_Region"]))
	for new_country in new_data["features"]:
		if (old_country["attributes"]["Country_Region"] == new_country["attributes"]["Country_Region"]):
			if ("Time_Of_Last_Change" not in old_country["attributes"]):
				new_country["attributes"]["Time_Of_Last_Change"] = round(timestamp)
				new_country["attributes"]["Second_From_Last_Change"] = 0
			else:
				old_confirmed = old_country["attributes"]["Confirmed"]
				old_recovered = old_country["attributes"]["Recovered"]
				old_death = old_country["attributes"]["Deaths"]
				new_confirmed = new_country["attributes"]["Confirmed"]
				new_recovered = new_country["attributes"]["Recovered"]
				new_death = new_country["attributes"]["Deaths"]
				if (old_confirmed < new_confirmed or old_recovered < new_recovered or old_death < new_death):
					print(slug(new_country["attributes"]["Country_Region"]) + ' changed')
					new_country["attributes"]["Time_Of_Last_Change"] = round(timestamp)
					new_country["attributes"]["Second_From_Last_Change"] = 0
				else:
					new_country["attributes"]["Second_From_Last_Change"] = round(timestamp - old_country["attributes"]["Time_Of_Last_Change"])
					new_country["attributes"]["Time_Of_Last_Change"] = old_country["attributes"]["Time_Of_Last_Change"]
#print(new_data)
new_data["features"].sort(key=confirm, reverse=True)
with open('_case_old.json', 'w') as outfile:
    json.dump(new_data, outfile)
